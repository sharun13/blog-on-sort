# **Sorting**


Ensuring that data is well organized can be crucial to performing many tasks. The arrangement of data in a specific order, usually ascending or descending, is termed as sorting. In case of any programming languages like JavaScript, there are different methods available to sort a given data. 

`sort` is the built-in function available in JavaScript for sorting. The `sort()` method sorts the elements of an array in place by interchanging the elements of an array. It is a Non-Destructive array method, as it doesn't mutate any data it is performing upon. 


## **Sorting Numbers**


The `sort()` method sorts the array elements in ascending order with the smallest value first and largest value last. By default, if not given any callback functions, `sort()` method converts the elements into strings and then compares the values. What this means is that if we have an array of numbers such as `numbers = [1,2,3,7,10,12]` and we directly use `numbers.sort()` the output we get will be `[1,10,12,2,3,7]`, because in string comparison `10` and `12` comes before 2,3 and 7.

To fix this, we can pass a callback function to the `sort()` method. The `sort()` method will use the callback function to determine the order of elements. The callback function of sort accepts two arguments. An example is given below.

```
let numbers = [1,2,3,7,10,12];
numbers.sort((first, second) => {
    if (first > second) {
        return 1;
    } else {
        return -1;
    }
});
```
What happens here is that, the callback function compares the two values and if they differ, then their positions will be interchanged and if not, their positions will be left unaltered. This snippet will give `[ 1, 2, 3, 7, 10, 12 ]` as output, which is what we want.


## **Sorting Strings**


As mentioned earlier, the default behavior of the JavaScript `sort()` function is to sort the elements based on the Unicode code point values of elements. To sort the elements of an array of strings in ascending order alphabetically, we can use the `sort()` method without passing the callback function. 

For example, if we want to sort an array of names, `['George', 'Alex', 'Elizebath', 'Charles', 'William']` in ascending order, we can directly call the sort function,

```
let names = ['George', 'Alex', 'Elizebath', 'Charles', 'William'];
names.sort();
```

This snippet will give the output
```
[ 'Alex', 'Charles', 'Elizebath', 'George', 'William' ]
```

If we want to use some other logic, say descending order of names, then we need to pass in a callback function that does a similar job as what the callback does in case of numbers. For example,

```
name.sort((first, second) => {
    if (first > second)
        return -1;
    if (first < second)
        return 1;
    return 0;
});
```

This will give the desired output - `[ 'William', 'George', 'Elizebath', 'Charles', 'Alex' ]

If we have an array that contains elements in both uppercase and lowercase, then we need to convert the elements into either uppercase or lowercase first before comparing, because uppercase and lowercase letters have unique unicode values. For that purpose, the built-in functions such as `toLowerCase` and `toUpperCase` can be made use of.


## **Sorting Complex Elements Such As Alphanumeric Strings Or Postal Codes**


As mentioned earlier, JavaScript internally executes the `sort()` by comparing the values in strings. But if we want some control over the sorting method and we want out data sorted in a specific format, we can use the string `localeCompare` method to compare strings so that we can sort them.

`localeCompare()` is an inbuilt method in JavaScript which is used to compare two strings and returns a positive number if the reference string is lexicographically greater than the compare string and negative number if the reference string is lexicographically smaller than the compare string and zero (0) if the compare and reference strings are equivalent.

Consider the example given below

```
const array = ['123asd','19asd','12345asd','asd123','asd12'];
const sortedArray = array.sort((first, second) => {
  return first.localeCompare(second, undefined, {
      numeric: true,
      sensitivity: 'base'
  });
});
console.log(sortedArray); // ["19asd", "123asd", "12345asd", "asd12", "asd123"]
```

Here the arguments of the `localCompare()` are, compare string - which is what we are comparing with the reference string, locale - lets us specify the language whose formatting conventions should be used, in this case we can use undefined and options - we set `numeric` to true to compare the numerical part of first and second, sensitivity is set to `base` to compare the case and the alphabet of the values. 

In case we want to sort the numbers that contains special characters, such as zipcodes, we can replace the special characters before making the comparisons. If the values are in string format, JavaScript makes the comparison as mentioned above and if the formatting of the zipcodes are not consistent, its better to convert it into number format by replacing the special characters and then work upon it. 


### **References**
- [MDN Web Docs LocaleCompare](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/localeCompare) 
- [MDN Web Docs Sort](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort)
 
